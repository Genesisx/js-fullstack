class Event {
  constructor(name) {
    this.name = name;
    // 库
    this.observers = {};
    // this.on = this.on.bind(this);
    // this.emit = this.emit.bind(this);
  }

  // emit
  // on
  on = (type, fn) => {
    if(this.observers[type]) {
      this.observers[type].push(fn);
    }else{
      this.observers[type] = [fn];
    }
  }

  emit = (type, opt) => {
    this.observers[type].map(item => [
      item(opt)
    ])
  }
}


