const path = require('path');
const express = require('express');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');

// const {User} = require('./sequelize');

const app = express();

app.use(bodyParser.json());
app.use(cookieParser());

// function loginRequired(req, res, next) {
//   const cookie = req.cookies;
//   const loginToken = cookie.loginToken;
//   console.log('loginToken', loginToken);
//   if (!loginToken) {
//     return res.redirect('/login');
//   }
//   const pageSize = req.query.pageSize;
//   const pageNumber = req.query.pageNumber;

//   User.findAndCountAll({
//     limit: +pageSize,
//     offset: +((pageNumber - 1) * pageSize),
//   }).then(result => {
//     console.log(result);
//     res.json(result);
//   });

//   User.findOne({
//     where: {
//       userId: loginToken
//     }
//   })
//   .then(userInfo => {
//     if (!userInfo) {
//       res.redirect('/login');
//     } else {
//       req.userInfo = userInfo;
//       next();
//     }
//   });
// }

app.get('/', function(req, res) {
    // 正常，发送的 send 里面的内容
    // res.send('hello world hello world hello world hello world hello world hello world hello world');
    // res.json({
    //   api: 'something'
    // });

    // // 正常，发送的 json 里面的内容
    // res.sendFile(path.resolve(__dirname, './login.html'));
    // res.json({
    //   api: 'something'
    // });

    // 不正常，内容是 login.html，content-length 是 index.html 的
    res.sendFile(path.resolve(__dirname, './login.html'));
    res.sendFile(path.resolve(__dirname, './index.json'));
    // res.setHeader('xx', 'xx');

    // // 正常，内容是 send 里面的 hello world
    // res.sendFile(path.resolve(__dirname, './login.html'));
    // res.send('hello world hello world hello world hello world hello world hello world hello world');

    // // 正常，内容是长的 hello world
    // res.send('hello world hello world hello world hello world hello world hello world');
    // res.send('hello world');
});



// app.get('/login', function(req, res) {
//   res.sendFile(path.resolve(__dirname, './login.html'));
// });

// app.post('/api/login', function(req, res) {
//   const {body} = req;
//   const {username, password} = body;
//   if (!username || !password) {
//     throw new Error('请输入用户名或密码');
//   }

//   console.log('用户名和密码是：', username, password);

//   User.findOne({
//     where: {
//       username,
//       password,
//     }
//   })
//   .then(userInfo => {
//     if (!userInfo) {
//       throw new Error('没有找到此用户');
//     }

//     res.cookie('loginToken', userInfo.userId, {
//       maxAge: 1000 * 60 * 15,
//       httpOnly: false
//     });

//     res.json({
//       data: {
//         redirectUrl: '/'
//       }
//     });
//   })
// });

app.listen(8080, function() {
  console.log('server start');
});
