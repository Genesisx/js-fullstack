const Sequelize = require('sequelize');
const UserModel = require('./model/user');

const sequelize = new Sequelize('node', 'root', 'password', {
  host: 'localhost',
  dialect: 'mysql'
});

sequelize
  .authenticate()
  .then(() => {
    console.log('success');
  })
  .catch(e => {
    console.log('error');
  });

const User = UserModel(sequelize, Sequelize.DataTypes);

User.sync({force: true})
  .then(() => {
    return User.bulkCreate(require('./data'))
  });

sequelize.User = User;

module.exports = sequelize;