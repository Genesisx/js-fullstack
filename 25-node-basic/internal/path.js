const path = require('path');

const resolvePath = path.resolve('..', 'a', '..', 'b', 'c/')
const joinPath = path.join('..', 'a', '..', 'b', 'c/')

console.log(resolvePath)
console.log(joinPath)

path.extname(__filename)
path.basename(__filename)
path.dirname(__filename)