const vm = require('vm');
const path = require('path');
const fs = require('fs');

function customRequire(filename) {
  const pathToFile = path.resolve(__dirname, filename);
  const content = fs.readFileSync(pathToFile, 'utf-8');

  const wrapper = [
    '(function(require, module, exports, __dirname, __filename, zhuawa) {',
    '})'
  ]

  const wrappedContent = wrapper[0] + content + wrapper[1];

  const script = new vm.Script(wrappedContent, {
    filename: 'index.js'
  });

  const module = {
    exports: {}
  };
  const result = script.runInThisContext();
  result(customRequire, module, module.exports);
  return module.exports;
}

global.r = customRequire;