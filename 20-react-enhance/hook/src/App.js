import React, { Component, useState, useEffect } from 'react';
import logo from './logo.svg';
import './App.css';

function useCheck() {
    const [num, setRandomText] = useState(Math.random(0, 1))
    if (num > 0.5) {
        return [num];
    }
    function setRandomTextHOC() {
        return setRandomText(Math.random(0, 1));
    }
    return [num, setRandomTextHOC];
}

function App() {
    const [num, setRandomText] = useCheck();
    return (
        <div className="App">
            <button onClick={setRandomText} disabled={!setRandomText}>
                {setRandomText ? '按钮' : '禁止点击'}
            </button>
            {num}
        </div>
    );
}

export default App;
