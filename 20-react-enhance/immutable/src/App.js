import React, { Component } from 'react';
import {cloneDeep} from 'lodash';
// import {produce} from 'immer';
import './App.css';

class Store {
    constructor(state) {
      this.modified = false
      this.source = state
      this.copy = null
    }
    get(key) {
      if (!this.modified) return this.source[key]
      return this.copy[key]
    }
    set(key, value) {
      if (!this.modified) this.modifing()
      return this.copy[key] = value
    }
    modifing() {
      if (this.modified) return
      this.modified = true
      this.copy = Array.isArray(this.source)
        ? this.source.slice()
        : { ...this.source }
    }
  }

const PROXY_FLAG = '@@SYMBOL_PROXY_FLAG'
const handler = {
  get(target, key) {
    if (key === PROXY_FLAG) return target
    return target.get(key)
  },
  set(target, key, value) {
    return target.set(key, value)
  },
}

function produce(state, producer) {
    const store = new Store(state)
    const proxy = new Proxy(store, handler)
  
    producer(proxy)
  
    const newState = proxy[PROXY_FLAG]
    if (newState.modified) return newState.copy
    return newState.source
  }


class RenderText extends React.Component {
  shouldComponentUpdate(prevProps) {
    console.log(this.props.store.key2 === prevProps.store.key2);
    return true;
  }
  render() {
    return (
      <div>
        <div>{this.props.store.key1}</div>
        <div>{this.props.store.key2.value}</div>
      </div>
    );
  }
}
const object = {
    key1: 'object.key1.value',
  key2: {value: 'object.key2.value', value2: 'object.key2.value2'}
};
class App extends Component {
  sendRandomText = () => {
    console.log(object.key1);
    this.forceUpdate();
  }
  render() {
      const obj = object;
    // const obj = cloneDeep(object);
    obj.key1 = Math.random(0, 1);
    // const obj = produce(object, (draft) => {
    //   draft.key1 = Math.random(0, 1);
    // });
    return (
      <div className="App">
        <button onClick={this.sendRandomText}>按钮</button>
        <RenderText store={obj} />
      </div>
    );
  }
}

export default App;
