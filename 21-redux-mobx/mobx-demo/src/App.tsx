import React from "react";
import "./App.css";
import { Todos } from "./mobx/Todo";
import { TodoApp } from "./components/TodoApp";

const todos = new Todos();
const todos2 = new Todos();

function App() {
  return <TodoApp todos={todos} />;
}

export default App;
