export const ADD_TODO = "add_todo";

export const ADD_TODO_REQUEST = "add_todo_request";
export const ADD_TODO_SUCCESS = "add_todo_success";
export const ADD_TODO_FAILURE = "add_todo_failure";

export const REMOVE_TODO = "remove_todo";

export const TOGGLE_TODO = "toggle_todo";
