const { StringSchema } = require("./string");

const schema = new StringSchema().length(12, "too long").min(6, "too short");

const errorMessage = validate("123123123123123", schema);

if (errorMessage) {
  console.log("error", errorMessage);
} else {
  console.log("correct");
}

function validate(value, schema) {
  return schema.isValid(value);
}
