class EventEmitter {
  constructor() {
    this._events = {};
  }

  on(name, cb) {
    if (!this._events[name]) {
      this._events[name] = [];
    }

    this._events[name].push(cb);

    // { data: [funciton] }
    // { data: [function, function]}
    // { data: [function, function, ...]}
  }

  emit(name, ...args) {
    if (!this._events[name]) {
      return;
    }

    // [function, function]
    for (const fn of this._events[name]) {
      fn.apply(null, args);
    }
  }

  off(name, cb) {
    // {}
    if (!this._events[name]) {
      return;
    }

    // { data: [function, function]}
    const index = this._events[name].findIndex((evt) => evt === cb);
    // 1 | 0 | -1
    if (index >= 0) {
      this._events[name].splice(index, 1);
      // index = 0
      // { data: [funciton] }
    }
  }
}

module.exports = {
  EventEmitter,
};
