// const { Eager } = require("./eager");
const { Lazy } = require("./lazy");

const ins1 = Lazy.getInstance();
Lazy.instance = null;
const ins2 = Lazy.getInstance();

// true
console.log(ins1 === ins2);
