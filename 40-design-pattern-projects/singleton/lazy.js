class Lazy {
  static #instance = null;

  static getInstance() {
    if (!Lazy.#instance) {
      Lazy.#instance = new Lazy("lazy");
    }
    return Lazy.#instance;
  }

  constructor(name) {
    console.log("lazy constructor", name);
    this.name = name;
  }
}

module.exports = {
  Lazy,
};

("use strict");

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true,
    });
  } else {
    obj[key] = value;
  }
  return obj;
}

class Lazy {
  static getInstance() {
    if (!Lazy.instance) {
      Lazy.instance = new Lazy("lazy");
    }

    return Lazy.instance;
  }

  constructor(name) {
    console.log("lazy constructor", name);
    this.name = name;
  }
}

_defineProperty(Lazy, "instance", null);

module.exports = {
  Lazy,
};
