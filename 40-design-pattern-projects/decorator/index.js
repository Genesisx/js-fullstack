const { decorator } = require("./decorator");
const { Developer } = require("./developer");

const developer = decorator(new Developer());

developer.send("frontend");
developer.send("backend");
developer.send("CEO");
developer.send("designer");
developer.send("QA");
