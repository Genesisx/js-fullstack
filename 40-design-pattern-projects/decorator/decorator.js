const decorator = (obj) => {
  obj.send = function (method, ...args) {
    if (!this[method]) {
      return this.methodMissing.apply(this, [method, ...args]);
    }
  };

  obj.methodMissing =
    obj.methodMissing ||
    function (...args) {
      console.log(...args);
    };

  return obj;
};

module.exports = {
  decorator,
};
