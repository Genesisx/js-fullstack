const express = require('express');
const path = require('path');
const fs = require('fs');
const React = require('react');
const ReactDOMServer = require('react-dom/server');
const app = express();

// const Component = React.createElement('div', null, 'hello world');
const Component = require('./src/Component.babel').default;

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  // res.header("Access-Control-Allow-Headers", "Content-Type,Content-Length, Authorization, Accept,X-Requested-With");
  // res.header("Access-Control-Allow-Methods","PUT,POST,GET,DELETE,OPTIONS");
  next();
});

app.get('/build/static/js/:filename', function(req, res) {
  const filename = req.params.filename;
  const pathToContent = path.resolve(__dirname, `build/static/js/${filename}`);
  res.send(fs.readFileSync(pathToContent, 'utf-8'));
});

app.get('/api/data', function(req, res) {
  const data = require(path.resolve(__dirname, './data.json'));
  setTimeout(function() {
    res.json(data);
  }, 5000);
});

app.get('/:router', function(req, res) {

});

app.get('/', function(req, res) {
  const data = {};
  // const content = ReactDOMServer.renderToString(React.createElement(Component, data));
  // console.log(content);
  const content = '';
  res.send(
    `
      <html>
        <body>
          <div id="root">${content}</div>
          <script>
            window.__init__ = ${JSON.stringify(data)}
          </script>
          <script src="/build/static/js/runtime-main.js"></script>
          <script src="/build/static/js/2.chunk.js"></script>
          <script src="/build/static/js/main.chunk.js"></script>
        </body>
      </html>
    `
  )
})

app.listen(8080);