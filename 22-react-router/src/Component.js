import React from 'react';

export default class Component extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: props.data || (window.__init__ && window.__init__.data)
    };
  }
  componentDidMount() {
    if (!this.state.data) {
      fetch('http://localhost:8080/api/data')
        .then(res => res.json())
        .then(res => {
          this.setState({data: res.data});
        });
    }
  }
  render() {
    const {data} = this.state;
    if (!data) {
      return 'loading...';
    }
    return (
      <div>{data}</div>
    );
  }
}