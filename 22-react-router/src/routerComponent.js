import React from 'react';
import { createBrowserHistory } from 'history';
import { match as matchPath} from 'path-to-regexp';

// matchPath('/about/:id', '/about/test');

const history = createBrowserHistory();

// const matchPath = (targetPath, currentPath) => targetPath === currentPath;

export class Route extends React.Component {
  componentWillMount() {
    history.listen((location, action) => {
      console.log('history.listen', location, action);
      this.forceUpdate();
    });
  }
  render() {
    const {
      path,
      component: Component,
      render,
    } = this.props;
    const pathname = window.location.pathname;
    const matcher = matchPath(path);
    const match = matcher(pathname);
    console.log(match);
    if (!match) return null;

    if (Component) {
      return (<Component match={match} />);
    }

    if (render) {
      return render({match});
    }

    return null;
  }
}

export class Link extends React.Component {
  handleClick = (e) => {
    e.preventDefault();
    history.push(this.props.to);
  }

  render() {
    const {
      to,
      children,
    } = this.props;
    return (
      <a href={to} onClick={this.handleClick}>{children}</a>
    );
  }
}