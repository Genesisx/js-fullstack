import React from 'react';
import { createBrowserHistory } from 'history';
import { match as matchPath } from 'path-to-regexp';

const history = createBrowserHistory();

// const matchPath = (currentPath, targetPath) => currentPath === targetPath;

export class Route extends React.Component {
  componentWillMount() {
    const unlisten = history.listen((location, action) => {
      console.log('history change listen', location, action);
      this.forceUpdate();
    });
    this.setState({unlisten});
  }

  componentWillUnmount() {
    const {unlisten} = this.state;
    unlisten();
  }

  render() {
    const {
      render,
      path,
      component: ChildComponent,
    } = this.props;

    const match = matchPath(path);
    const matchResult = match(window.location.pathname);

    if (!matchResult) return null;

    if (ChildComponent) {
      return (<ChildComponent match={matchResult} />);
    }

    if (render && typeof render === 'function') {
      return render({matchResult});
    }

    return null;
  }
}

export class Link extends React.Component {
  handleClick = (e) => {
    const {
      replace,
      to,
    } = this.props;
    e.preventDefault();
    replace ? history.replace(to) : history.push(to);
  }

  render() {
    const {
      to,
      children,
    } = this.props;

    return (
      <a href={to} onClick={this.handleClick}>{children}</a>
    );
  }
}