const path = require('path');
const fs = require('fs');
const express = require('express');
const React = require('react');

const ReactDOMServer = require('react-dom/server');
const app = express();

// const Component = React.createElement('div', null, 'hello world');
const Component = require('./src/fetchDataComponent');
const data = {
  data: '服务端数据'
};
app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  next();
});

app.get('/api/data', function(req, res) {
  setTimeout(function() {
    res.json(data);
  }, 2000);
})

app.get('/build/static/js/:filename', function(req, res) {
  const filename = req.params.filename;
  const pathToFile = path.resolve(__dirname, `build/static/js/${filename}`);
  const content = fs.readFileSync(pathToFile, 'utf-8');
  res.send(content);
});


app.get('/', function(req, res) {
  // const componentStr = ReactDOMServer.renderToString(
  //   React.createElement(Component, data)
  // );
  // const dataStr = JSON.stringify(data);
  const componentStr = '';
  const dataStr = '';
  console.log('server get /', componentStr);
  res.send(
    `
      <html>
        <body>
          <div id='root'>${componentStr}</div>
          <script>window.__init__=${dataStr}</script>
          <script src="/build/static/js/runtime-main.js"></script>
          <script src="/build/static/js/2.chunk.js"></script>
          <script src="/build/static/js/main.chunk.js"></script>
        </body>
      </html>
    `
  );
});

app.listen(8080);